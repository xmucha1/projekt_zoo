//
// Created by Tomas on 17.05.2020.
//
#include "Zivocich.h"
#include "Mapa.h"
#include <iostream>

Zivocich *Zivocich::vytvor(KonkretniZvire nejakeZvire)
{
    switch (nejakeZvire)
    {
        case KonkretniZvire::Pes:
            return new Zivocich(5, 4, 5, 5, 5, 5, 5, 5, Potrava::Masozravec, 'P');
            break;
        case KonkretniZvire::Kocka:
            return new Zivocich(5, 4, 5, 4, 4, 6, 5, 4, Potrava::Masozravec, 'K');
            break;
        case KonkretniZvire::Krokodil:
            return new Zivocich(5, 4, 5, 6, 6, 5, 5, 5, Potrava::Masozravec, 'K');
            break;
        case KonkretniZvire::Jesterka:
            return new Zivocich(5, 4, 5, 2, 2, 5, 5, 5, Potrava::Bylozravec, 'J');
            break;
        default:
            return nullptr;
    }
}

Zivocich::Zivocich(int pocatecniHlad, int pocatecniZizen, int maxVzdalenost, int velikost, int sila, int obratnost, int hladovost, int rychlost, Potrava potrava, char symbol)
{
    m_hlad = pocatecniHlad;
    m_zizen = pocatecniZizen;
    m_maxVzdalenost = maxVzdalenost;
    m_velikost = velikost;
    m_sila = sila;
    m_obratnost = obratnost;
    m_hladovost = hladovost;
    m_rychlost = rychlost;
    m_potrava = potrava;
    m_symbol = symbol;
    m_posun = false;
}

bool Zivocich::getPosun()
{
    return m_posun;
}

void Zivocich::setPosun(bool posun)
{
    m_posun = posun;
}

int Zivocich::getZizen()
{
    return m_zizen;
}

int Zivocich::getHlad()
{
    return m_hlad;
}

int Zivocich::getMaxVzdalenost()
{
    return m_maxVzdalenost;
}

int Zivocich::getVelikost()
{
    return m_velikost;
}

int Zivocich::getSila()
{
    return m_sila;
}

int Zivocich::getObratnost()
{
    return m_obratnost;
}

int Zivocich::getHladovost()
{
    return m_hladovost;
}

int Zivocich::getRychlost()
{
    return m_rychlost;
}

Potrava Zivocich::getPotrava()
{
    return m_potrava;
}

void Zivocich::setZizen(int piti)
{
    m_zizen = piti;
}

void Zivocich::setHlad(int jidlo)
{
    m_hlad = jidlo;
}

Zivocich::~Zivocich()
{
    //std::cout << "Zemrel zivocich" << std::endl;
}

void Zivocich::jist(Zivocich *korist)
{
    m_velikost++;
    m_hlad += korist->getVelikost();
    delete korist;
}

void Zivocich::printZnak()
{
    std::cout << "*";
    std::cout << m_symbol;
    std::cout << "*";
}
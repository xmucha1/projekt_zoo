//
// Created by Tomas on 16.05.2020.
//
#include "MapaDirectory.h"

MapaDirectory::MapaDirectory(MapaBuilder *boardBuilder)
{
    m_mapaBuilder = boardBuilder;

}

void MapaDirectory::setMapaBuilder(MapaBuilder *novyBuilder)
{
    m_mapaBuilder = novyBuilder;

}

Mapa *MapaDirectory::vytvorMapu(unsigned int sirka, unsigned int vyska)
{
    //sekvence tvoreni mapy
    m_mapaBuilder->vytvoritMapu(sirka, vyska, TypHerniPole::Louka);
    m_mapaBuilder->GenerovatKrajinu();
    m_mapaBuilder->GenerovatZvirata();
    //zde generovani zvirat atd.
    return m_mapaBuilder->getMapa();
}

Mapa *MapaDirectory::ZahrajJednoKolo(unsigned int maxPosun)
{
    m_mapaBuilder->posunNahodneZvirata(maxPosun);
    return m_mapaBuilder->getMapa();
}
//
// Created by Tomas on 16.05.2020.
//
#include "Mapa.h"

Mapa::Mapa(unsigned int sirka, unsigned int vyska, TypHerniPole zakladniHodnota)
{
    HerniPole defaultTile = HerniPole::vytvor(zakladniHodnota);
    std::vector<HerniPole> tmpRow(sirka, defaultTile);
    //lze upravit
    for (int i = 0; i < vyska; ++i)
    {
        m_mapa.push_back(tmpRow);
        //m_mapaZvirat.push_back(tmpRow);
    }

    //Zivocich* defaultTileZvire = Zivocich::vytvor(KonkretniZvire::nic);
    std::vector<Zivocich*> tmpRow2(sirka, nullptr);
    //lze upravit
    for (int i = 0; i < vyska; ++i)
    {
        m_mapaZvirat.push_back(tmpRow2);

    }

    /*
    for (int i = 0; i < m_mapaZvirat.size(); ++i) {
        std::cout<<"|";
        for (int j = 0; j < m_mapaZvirat.at(i).size(); ++j) {
            m_mapaZvirat.at(i).at(j)->printZnak();
        }

        std::cout<<"|"<<std::endl;
    }*/

}

void Mapa::setHerniPole(HerniPole tile, unsigned int row, unsigned int col)
{
    m_mapa.at(row).at(col) = tile;
}

bool Mapa::testRozsahuMapa(unsigned int row, unsigned int col)
{
    if ((row < m_mapa.size()) && (col < m_mapa.at(0).size()))
    {
        //osetreni  proti indexovanim mimo rozsah
        return true;
    }
    else
    {
        return false;
    }
}

int Mapa::getPocetZvirat()
{
    int pocet = 0;
    for (int i = 0; i < m_mapaZvirat.size(); ++i)
    {
        for (int j = 0; j < m_mapaZvirat.at(i).size(); ++j)
        {
            if (m_mapaZvirat.at(i).at(j) != nullptr)
            {
                pocet++;
            }
            else
            {
                pocet = pocet;
            }

            //std::cout<<j<<std::endl;
        }
    }

    return pocet;
}

void Mapa::setZvireNaMapu(Zivocich *tile, unsigned int row, unsigned int col)
{
    m_mapaZvirat.at(row).at(col) = tile;

}

bool Mapa::testRozsahuZvirata(unsigned int row, unsigned int col)
{
    if ((row < m_mapaZvirat.size()) && (col < m_mapaZvirat.at(0).size()))
    {
        //osetreni  proti indexovanim mimo rozsah
        return true;
    }
    else
    {
        return false;
    }
}

HerniPole Mapa::getHerniPole(unsigned int row, unsigned int col)
{
    return m_mapa.at(row).at(col);

}

Zivocich *Mapa::getZvire(unsigned int row, unsigned int col)
{
    return m_mapaZvirat.at(row).at(col);

}

void Mapa::ZobrazitMapu()
{
    for (int i = 0; i < m_mapa.size(); ++i)
    {
        std::cout << "|";
        for (int j = 0; j < m_mapa.at(i).size(); ++j)
        {
            m_mapa.at(i).at(j).printZnak();
        }

        std::cout << "|" << std::endl;
    }
}

void Mapa::ZobrazitMapuZvirat()
{
    for (int i = 0; i < m_mapaZvirat.size(); ++i)
    {
        std::cout << "|";
        for (int j = 0; j < m_mapaZvirat.at(i).size(); ++j)
        {
            if (m_mapaZvirat.at(i).at(j) != nullptr)
            {
                m_mapaZvirat.at(i).at(j)->printZnak();
            }
            else
            {
                std::cout << "   ";
            }
        }

        std::cout << "|" << std::endl;
    }
}

void Mapa::ZbrazitKompletniMapu()
{
    for (int i = 0; i < m_mapa.size(); ++i)
    {
        std::cout << "|";
        for (int j = 0; j < m_mapa.at(i).size(); ++j)
        {
            if (m_mapaZvirat.at(i).at(j) == nullptr)
            {
                m_mapa.at(i).at(j).printZnak();
            }
            else
            {
                m_mapaZvirat.at(i).at(j)->printZnak();
            }
        }

        std::cout << "|" << std::endl;
    }
}

void Mapa::setPosunToZvire(bool posun, unsigned int row, unsigned int col)
{
    m_mapaZvirat.at(row).at(col)->setPosun(posun);

}

bool Mapa::getPosunFromZvire(bool posun, unsigned int row, unsigned int col)
{
    return m_mapaZvirat.at(row).at(col)->getPosun();
}
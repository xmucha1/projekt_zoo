//
// Created by Tomas on 30.05.2020.
//
#include "Hrej.h"

void Hrej::HrejHru()
{
    unsigned int pocetKol, maxPosun, vyskaMapy, sirkaMapy;
    std::cout << "Menu:" << std::endl;

    std::cout << "Zadejte velikost mapy (sirka vyska):  ";
    std::cin >> sirkaMapy;
    std::cin >> vyskaMapy;
    std::cout << "Zadejte pocet kol: ";
    std::cin >> pocetKol;
    std::cout << "Zadejte maxilani posun za 1 kolo: ";
    std::cin >> maxPosun;
    while (maxPosun > sirkaMapy or maxPosun > vyskaMapy)
    {
        std::cout << "Posun nemuze byt vetsi nez sirka a vyska mapy. Zadejte vyhovujici posun: ";
        std::cin >> maxPosun;
    }

    Hraj(maxPosun, pocetKol, sirkaMapy, vyskaMapy);

    //Hraj(1,6, 10, 10);

}

void Hrej::Hraj(unsigned int maxPosun, unsigned int pocetkol, unsigned int sirkaMapy, unsigned int vyskaMapy)
{
    //pocitani kol..

    MapaDirectory *spravce = new MapaDirectory(new NahodnaMapa());
    Mapa *mapa = spravce->vytvorMapu(sirkaMapy, vyskaMapy);

    for (int i = 0; i < pocetkol; ++i)
    {
        // s kazdym kolem je potreba odebrat jilo a piti + zjistit policko a aplikovat jeko efekt na zvire!!!
        std::cout << "\n";
        std::cout << "--------------------------------------------------------------------------------------" << std::endl;
        std::cout << "Kolo cislo: " + std::to_string(i + 1) << std::endl;
        std::cout << "Pocet zvirat na zacatku kola: " + std::to_string(mapa->getPocetZvirat()) << std::endl;
        mapa->ZbrazitKompletniMapu();
        std::cout << "\n";
        mapa = spravce->ZahrajJednoKolo(maxPosun);
        std::cout << "Pocet zvirat na konci kola : " + std::to_string(mapa->getPocetZvirat()) << std::endl;
        mapa->ZbrazitKompletniMapu();
        std::cout << "--------------------------------------------------------------------------------------" << std::endl;

    }

    delete mapa;
    delete spravce;

}	// nastavit posun na false, vygenerovat smer, posunout o zadane mnozstvi policek (pokud je to mozne), nastavyt posun na true..

Zivocich *Hrej::interaguj(Zivocich *kdo, Zivocich *sKym)
{
    if (kdo->getPotrava() == Potrava::Bylozravec && sKym->getPotrava() == Potrava::Masozravec)
    {
        if (sKym->getVelikost() > kdo->getVelikost() and(sKym->getObratnost() >= kdo->getObratnost() or sKym->getRychlost() >= kdo->getRychlost()))
        {
            sKym->jist(kdo);
            std::cout << "zemrel na sezrani M/B" << std::endl;

            return sKym;
        }
    }
    else if (kdo->getPotrava() == Potrava::Masozravec && sKym->getPotrava() == Potrava::Bylozravec)
    {
        if (kdo->getVelikost() > sKym->getVelikost() and(kdo->getObratnost() >= sKym->getObratnost() or kdo->getRychlost() >= sKym->getRychlost()))
        {
            kdo->jist(sKym);

            std::cout << "zemrel na sezrani M/B" << std::endl;

            return kdo;
        }
    }
    else if (kdo->getPotrava() == Potrava::Bylozravec && sKym->getPotrava() == Potrava::Bylozravec)
    {
        if (kdo->getSila() >= sKym->getSila())
        {
            delete sKym;
            std::cout << "zemrel na zabiti B/B" << std::endl;
            return kdo;
        }
        else
        {
            delete kdo;
            std::cout << "zemrel na zabiti2 B/B" << std::endl;

            return sKym;
        }
    }
    else if (kdo->getPotrava() == Potrava::Masozravec && sKym->getPotrava() == Potrava::Masozravec)
    {
        if (sKym->getVelikost() > kdo->getVelikost() and(sKym->getObratnost() >= kdo->getObratnost() or sKym->getRychlost() >= kdo->getRychlost()))
        {
            sKym->jist(kdo);

            std::cout << "zemrel na sezrani M/M" << std::endl;

            return sKym;
        }
        else if (kdo->getVelikost() > sKym->getVelikost() and(kdo->getObratnost() >= sKym->getObratnost() or kdo->getRychlost() >= sKym->getRychlost()))
        {
            kdo->jist(sKym);

            std::cout << "zemrel na sezrani2 M/M" << std::endl;

            return kdo;
        }
        else
        {
            // pokud jsou oba stejneho druhu tak se vrati ten prni..
            //std::cout<< ""<<std::endl;
            return kdo;

        }
    }
    else
    {
        std::cout << "interakce neco jineho" << std::endl;

    }

    std::cout << "interakce neco jineho" << std::endl;

}

Zivocich *Hrej::interagujSMapou(Zivocich *zvire, HerniPole pole)
{
    if (zvire->getPotrava() == Potrava::Bylozravec)
    {
        int hlad;
        if (zvire->getHlad() + pole.getFoodEffect() > 100)
        {
            hlad = 100;
        }
        else
        {
            hlad = zvire->getHlad() + pole.getFoodEffect();
        }

        zvire->setHlad(hlad);

    }

    int zizen;

    if (zvire->getZizen() + pole.getWaterEffect() > 100)
    {
        zizen = 100;

    }
    else
    {
        zizen = zvire->getZizen() + pole.getWaterEffect();
    }

    zvire->setZizen(zizen);

    if (zvire->getHlad() <= 0 || zvire->getZizen() <= 0)
    {
        delete zvire;
        std::cout << "zemrel na jilo nebo piti" << std::endl;
        return nullptr;
    }

    return zvire;

}

//return kdo prezije ?kdo dostane policko, kdyz se potkaji 2 bykizravci?? Asi ten silnejsi nebo nevim..
// interakce s ostatnimy zviraty + ?efekt policka?
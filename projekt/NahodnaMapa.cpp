//
// Created by Tomas on 16.05.2020.
//
#include "NahodnaMapa.h"

void NahodnaMapa::GenerovatKrajinu()
{
    srand((unsigned) time(NULL));

    int somenumber = 0;
    HerniPole someTile;
    int x = 0, y = 0;

    do {
        y = -1;
        while (m_mapa->testRozsahuMapa(x, ++y))
        {
            somenumber = rand() % 50 + 1;
            if (somenumber < 10)
            {
                someTile = HerniPole::vytvor(TypHerniPole::Voda);
                m_mapa->setHerniPole(someTile, x, y);
            }
            else if (somenumber >= 10 && somenumber < 20)
            {
                someTile = HerniPole::vytvor(TypHerniPole::Pustina);
                m_mapa->setHerniPole(someTile, x, y);
            }
            else if (somenumber >= 20 && somenumber < 30)
            {
                someTile = HerniPole::vytvor(TypHerniPole::TravataLouka);
                m_mapa->setHerniPole(someTile, x, y);
            }
            else
            {
                someTile = HerniPole::vytvor(TypHerniPole::Louka);
                m_mapa->setHerniPole(someTile, x, y);
            }
        }

        y = 0;
    } while (m_mapa->testRozsahuMapa(x++, y));

}

void NahodnaMapa::GenerovatZvirata()
{
    srand((unsigned) time(NULL));

    int somenumber = 0;
    int x = 0, y = 0;

    do {
        y = -1;
        while (m_mapa->testRozsahuZvirata(x, ++y))
        {
            somenumber = rand() % 15;	// cislem se nastavuje hustova zvirat min: 5 !

            if (0 == somenumber)
            {
                m_mapa->setZvireNaMapu(Zivocich::vytvor(KonkretniZvire::Krokodil), x, y);
            }
            else if (1 == somenumber)
            {
                m_mapa->setZvireNaMapu(Zivocich::vytvor(KonkretniZvire::Pes), x, y);
            }
            else if (2 == somenumber)
            {
                m_mapa->setZvireNaMapu(Zivocich::vytvor(KonkretniZvire::Kocka), x, y);
            }
            else if (4 == somenumber)
            {
                m_mapa->setZvireNaMapu(Zivocich::vytvor(KonkretniZvire::Jesterka), x, y);
            }
            else
            {
                m_mapa->setZvireNaMapu(nullptr, x, y);
            }
        }

        y = 0;
    } while (m_mapa->testRozsahuZvirata(++x, y));
}

void NahodnaMapa::posunNahodneZvirata(unsigned int maxPosun)
{
    int x = 0, y = 0;
    int row = 0, col = 0;
    int noveX = 0, noveY = 0;
    int rozsahPoleX = 0, rozsahPoleY = 0;

    while (m_mapa->testRozsahuZvirata(x++, y))
    {
        rozsahPoleX++;
    }

    x = 0;
    while (m_mapa->testRozsahuZvirata(x, y++))
    {
        rozsahPoleY++;
    }

    //std::cout<<"X: " + std::to_string(rozsahPoleX)<<std::endl;
    //std::cout<<"Y: " + std::to_string(rozsahPoleY)<<std::endl;

    x = 0;
    y = 0;

    do {
        //std::cout << "radky"<< std::endl;
        y = -1;
        while (m_mapa->testRozsahuZvirata(x, ++y))
        {
            if (m_mapa->getZvire(x, y) != nullptr)
            {
                Zivocich *someZvire = m_mapa->getZvire(x, y);

                if (someZvire->getPosun() == false)
                {
                    someZvire->setPosun(true);
                    someZvire = Hrej::interagujSMapou(m_mapa->getZvire(x, y), m_mapa->getHerniPole(x, y));
                    m_mapa->setZvireNaMapu(someZvire, x, y);

                    row = NahodnaMapa::generovatSmerPosunu() *maxPosun;
                    col = NahodnaMapa::generovatSmerPosunu() *maxPosun;

                    if (row + x < 0)
                    {
                        noveX = 0;

                    }
                    else if (row + x < rozsahPoleX)
                    {
                        noveX = row + x;
                    }
                    else
                    {
                        noveX = rozsahPoleX - 1;
                    }

                    if (col + y < 0)
                    {
                        noveY = 0;

                    }
                    else if (col + y < rozsahPoleY)
                    {
                        noveY = col + y;
                    }
                    else
                    {
                        noveY = rozsahPoleY - 1;
                    }

                    //std::cout << "X: " + std::to_string(noveX)+ "; " + std::to_string(x) << std::endl;
                    //std::cout << "Y: " + std::to_string(noveY)+ "; "  + std::to_string(y) << std::endl;
                    if (noveX != x && noveY != y && m_mapa->getZvire(x, y) != nullptr)
                    {
                        if (m_mapa->getZvire(noveX, noveY) != nullptr)
                        {
                            //posun na obsazene pole
                            //interakce();// pokud m_posun je ture, jinak jeste muze odejit z policka

                            m_mapa->setZvireNaMapu(Hrej::interaguj(m_mapa->getZvire(x, y), m_mapa->getZvire(noveX, noveY)), noveX, noveY);
                            m_mapa->setZvireNaMapu(nullptr, x, y);

                        }
                        else
                        {
                            //posun na volne pole
                            m_mapa->setZvireNaMapu(someZvire, noveX, noveY);
                            m_mapa->setZvireNaMapu(nullptr, x, y);

                        }
                    }
                }
                else
                {
                    // std::cout << "posun je true" << std::endl;

                }
            }
        }

        y = 0;
    } while (m_mapa->testRozsahuZvirata(++x, y));

    setAllPosunToFalse();
    //std::cout << "konec posouvani" << std::endl;

}

int NahodnaMapa::generovatSmerPosunu()
{
    int somenumber = rand() % 3;
    //std::cout << std::to_string(somenumber) << std::endl;

    if (somenumber == 0)
    {
        //std::cout << "zaporne" << std::endl;

        return -1;
    }
    else if (somenumber == 1)
    {
        return 0;
    }
    else
    {
        return 1;
    }
}

void NahodnaMapa::setAllPosunToFalse()
{
    int x = 0, y = 0;

    do {
        // std::cout << x << std::endl;

        y = -1;
        while (m_mapa->testRozsahuMapa(x, ++y))
        {
            //  std::cout << y << std::endl;
            if (m_mapa->getZvire(x, y) != nullptr)
            {
                m_mapa->getZvire(x, y)->setPosun(false);
            }
        }

        y = 0;
    } while (m_mapa->testRozsahuMapa(x++, y));
}
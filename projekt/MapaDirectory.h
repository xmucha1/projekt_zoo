//
// Created by Tomas on 16.05.2020.
//

#ifndef PROJEKT_MAPADIRECTORY_H
#define PROJEKT_MAPADIRECTORY_H
#include "Mapa.h"
#include "MapaBuilder.h"

class MapaDirectory
{
    MapaBuilder * m_mapaBuilder;

public:
    MapaDirectory(MapaBuilder *mapaBuilder);
    void setMapaBuilder(MapaBuilder *novyBuilder);
    Mapa* ZahrajJednoKolo(unsigned int maxPosun);
    Mapa* vytvorMapu(unsigned int sirka, unsigned int vyska);
};

#endif	//PROJEKT_MAPADIRECTORY_H
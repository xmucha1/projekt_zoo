//
// Created by Tomas on 16.05.2020.
//

#include "MapaBuilder.h"

void MapaBuilder::vytvoritMapu(unsigned int sirka, unsigned int vyska, TypHerniPole zakladniHodnota){
    m_mapa = new Mapa(sirka, vyska, zakladniHodnota);
}

Mapa* MapaBuilder::getMapa(){
    return m_mapa;
}
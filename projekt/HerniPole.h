//
// Created by Tomas on 16.05.2020.
//

#ifndef PROJEKT_HERNIPOLE_H
#define PROJEKT_HERNIPOLE_H
#include <iostream>

enum class TypHerniPole
{
    Voda,
    Pustina,
    TravataLouka,
    Louka
};

struct HerniPole
{
    int m_waterEffect;
    int m_foodEffect;
    char m_symbol;

    static HerniPole vytvor(TypHerniPole typ);
    int getFoodEffect();
    int getWaterEffect();
    void printInfo();
    void printZnak();
};

#endif	//PROJEKT_HERNIPOLE_H
//
// Created by Tomas on 16.05.2020.
//

#include "HerniPole.h"


HerniPole HerniPole::vytvor(TypHerniPole typ) {

    switch (typ) {
        case TypHerniPole::Pustina :
            return HerniPole{-5,0, 'p'};
            break;
        case TypHerniPole::Voda :
            return HerniPole{10,0, 'v'};
            break;
        case TypHerniPole::TravataLouka :
            return HerniPole{4,10, 't'};
            break;
        case TypHerniPole::Louka :
            return HerniPole{0,0, ' '};
            break;
        default:
            return HerniPole{0,0, ' '};

    }
}
void HerniPole::printInfo(){
    std::cout<< "Effect na jidlo: " + std::to_string(m_foodEffect) + ", Effect na piti: " + std::to_string(m_waterEffect) <<std::endl;

}

void HerniPole::printZnak(){
    std::cout<<" ";
    std::cout<<m_symbol;
    std::cout<<" ";
}
int HerniPole::getFoodEffect() {
    return m_foodEffect;
}
int HerniPole::getWaterEffect() {
    return m_waterEffect;
}

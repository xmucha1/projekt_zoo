//
// Created by Tomas on 16.05.2020.
//

#ifndef PROJEKT_MAPA_H
#define PROJEKT_MAPA_H
#include <vector>
#include "HerniPole.h"
#include <iostream>
#include "Zivocich.h"

class Mapa
{
    std::vector<std::vector < HerniPole>> m_mapa;
    std::vector<std::vector<Zivocich*>> m_mapaZvirat;
public:
    Mapa(unsigned int sirka, unsigned int vyska, TypHerniPole zakladniHodnota);
    void setHerniPole(HerniPole tile, unsigned int row, unsigned int col);
    bool testRozsahuMapa(unsigned int row, unsigned int col);

    void setZvireNaMapu(Zivocich *tile, unsigned int row, unsigned int col);
    bool testRozsahuZvirata(unsigned int row, unsigned int col);

    void setPosunToZvire(bool posun, unsigned int row, unsigned int col);
    bool getPosunFromZvire(bool posun, unsigned int row, unsigned int col);

    HerniPole getHerniPole(unsigned int row, unsigned int col);
    Zivocich* getZvire(unsigned int row, unsigned int col);
    void ZobrazitMapu();
    void ZobrazitMapuZvirat();
    void ZbrazitKompletniMapu();
    int getPocetZvirat();
};

#endif	//PROJEKT_MAPA_H
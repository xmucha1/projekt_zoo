//
// Created by Tomas on 16.05.2020.
//

#ifndef PROJEKT_MAPABUILDER_H
#define PROJEKT_MAPABUILDER_H
#include "Mapa.h"

class MapaBuilder
{
protected:
    Mapa * m_mapa;

public:
    void vytvoritMapu(unsigned int sirka, unsigned int vyska, TypHerniPole zakladniHodnota);

    virtual void GenerovatKrajinu() = 0;
    virtual void GenerovatZvirata() = 0;
    virtual void posunNahodneZvirata(unsigned int maxPosun) = 0;

    //virtual ~MapaBuilder();
    Mapa* getMapa();
};

#endif	//PROJEKT_MAPABUILDER_H
//
// Created by Tomas on 30.05.2020.
//

#ifndef PROJEKT_HREJ_H
#define PROJEKT_HREJ_H
#include <iostream>
#include "Mapa.h"
#include "MapaDirectory.h"
#include "NahodnaMapa.h"
#include "Zivocich.h"

class Hrej
{
    // unsigned int m_maxPosun;

    Hrej();	// bez konstruktoru
public:
    static void HrejHru();	// prostredi uzivatel, zada kolik zvirat generovat a max posun
    static void Hraj(unsigned int maxPosun, unsigned int pocetKol, unsigned int sirkaMapy, unsigned int vyskaMapy);	// nastavit posun na false, vygenerovat smer, posunout o zadane mnozstvi policek (pokud je to mozne), nastavit posun na true..
    static Zivocich* interaguj(Zivocich *kdo, Zivocich *sKym);	// interakce s ostatnimy zviraty + ?efekt policka?
    static Zivocich* interagujSMapou(Zivocich *zvire, HerniPole pole);
};

#endif	//PROJEKT_HREJ_H
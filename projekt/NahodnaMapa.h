//
// Created by Tomas on 16.05.2020.
//

#ifndef PROJEKT_NAHODNAMAPA_H
#define PROJEKT_NAHODNAMAPA_H
#include "MapaBuilder.h"
#include <stdlib.h>
#include <time.h>
#include "Zivocich.h"
#include "Hrej.h"

class NahodnaMapa: public MapaBuilder
{
    int generovatSmerPosunu();
    void setAllPosunToFalse();
public:
    void GenerovatKrajinu();
    void GenerovatZvirata();
    void posunNahodneZvirata(unsigned int maxPosun);
    ~NahodnaMapa();
};

#endif	//PROJEKT_NAHODNAMAPA_H
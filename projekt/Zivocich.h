//
// Created by Tomas on 17.05.2020.
//

#ifndef PROJEKT_ZIVOCICH_H
#define PROJEKT_ZIVOCICH_H

enum class KonkretniZvire
{
    Pes,
    Kocka,
    Krokodil,
    Jesterka,
};

enum class Potrava
{
    Masozravec,
    Bylozravec
};

class Zivocich
{
    int m_hlad;
    int m_zizen;
    int m_maxVzdalenost;
    int m_velikost;
    int m_sila;
    int m_obratnost;
    int m_hladovost;
    int m_rychlost;
    Potrava m_potrava;
    char m_symbol;
    bool m_posun;

    Zivocich(int pocatecniHlad, int pocatecniZizen, int maxVzdalenost, int velikost, int sila, int obratnost, int hladovost, int rychlost, Potrava potrava, char symbol);
public:

    void setPosun(bool posun);
    bool getPosun();
    void printZnak();
    int getZizen();
    int getHlad();
    int getMaxVzdalenost();
    int getVelikost();
    int getSila();
    int getObratnost();
    int getHladovost();
    int getRychlost();
    Potrava getPotrava();
    void setZizen(int piti);
    void setHlad(int jidlo);
    void jist(Zivocich *korist);
    ~Zivocich();

    //Tovartni metoda
    static Zivocich* vytvor(KonkretniZvire nejakeZvire);

};

#endif	//PROJEKT_ZIVOCICH_H
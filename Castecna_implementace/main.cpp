#include <iostream>

using namespace std;


enum class Potrava{
    Bylozravec,
    Masozravec
};

struct DRUH{
    int sila;
    int obratnost;
    int rychlost;
    int hladovost;
    int maxVzdalenost;
    int velikost;
};
DRUH Zajic = {1,2,4,3,3,3};
DRUH Pes = {8,4,2,9,8,5};
DRUH Orel = {3,5,9,3,8,4};
DRUH holub = {1,6,8,3, 3,1};



class Zvire{
private:
    int m_hlad;
    int m_zizen;
    int m_maxVzdalenost;
    int m_velikost;

public:

    Zvire(int maxVzdalenost, int velikost){
        m_hlad = 0;
        m_zizen = 0;
        m_maxVzdalenost = maxVzdalenost;
        m_velikost = velikost;
    }

    int getZizen(){
        return m_zizen;

    }

    int getHlad(){
        return m_hlad;

    }

    int getMaxVzdalenost(){
        return m_maxVzdalenost;

    }

    int getVelikost(){
        return m_velikost;

    }

    void pit(){

    }


    //ruzne druhy maji ruzne potravy
    void virtual jist(){
        cout << "jist nejake zvire"<<endl;
    }

    string virtual getSkupinaPotrava(){
        cout << "jim jeste nevim co"<<endl;
        return "";
    }

};

class Druh: public Zvire{
private:
    int m_sila;
    int m_obratnost;
    int m_hladovost;
    int m_rychlost;


public:
    Druh(int sila, int obratnost, int hladovost, int rychlost, int maxVzdalenost, int velikost):Zvire(maxVzdalenost, velikost){
        m_sila = sila;
        m_obratnost = obratnost;
        m_hladovost = hladovost;
        m_rychlost = rychlost;
    }

    int getSila(){
        return m_sila;
    }

};


class Dravec: public Druh{


public:
    Dravec(int maxVzdalenost, int velikost, int sila, int obratnost, int hladovost, int rychlost):Druh(sila, obratnost, hladovost, rychlost, maxVzdalenost, velikost){

    }

    void jist(){
        cout << "jist Dravec"<<endl;
    }

    string getSkupinaPotrava(){
        cout << "jim maso"<<endl;
        return "jim maso";
    }

};

class Bylozravec: public Druh{




public:
    Bylozravec(int maxVzdalenost, int velikost, int sila, int obratnost, int hladovost, int rychlost):Druh(sila, obratnost, hladovost, rychlost, maxVzdalenost, velikost){

    }

    void jist(){
        cout << "jist Bylozravec"<<endl;
    }
    string getSkupinaPotrava(){
        cout << "jim zeleny"<<endl;
        return "jim zeleny";
    }
};





class FactoryZvire{
private:
    FactoryZvire(){

    }
public:
    static Zvire* vytvorNoveZvire(Potrava skupinaPotray, DRUH Zvire) {

        if (skupinaPotray == Potrava::Bylozravec) {
            return new Bylozravec(Zvire.maxVzdalenost,Zvire.velikost, Zvire.sila, Zvire.obratnost, Zvire.hladovost, Zvire.rychlost);

        }
        if (skupinaPotray == Potrava::Masozravec) {
            return new Dravec(Zvire.maxVzdalenost,Zvire.velikost, Zvire.sila, Zvire.obratnost, Zvire.hladovost, Zvire.rychlost);

        }else{
            return new Dravec(Zvire.maxVzdalenost,Zvire.velikost, Zvire.sila, Zvire.obratnost, Zvire.hladovost, Zvire.rychlost);

        }

    }



};




int main() {

    Zvire* karel = nullptr;

    karel = FactoryZvire::vytvorNoveZvire(Potrava::Masozravec, Pes);

    karel->jist();
    karel->getSkupinaPotrava();
    cout<< karel->getVelikost()<<endl;
    cout << "dajshd" << endl;
    return 0;
}
